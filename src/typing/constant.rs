use derive_more::From;

use crate::utils::Bore;
use super::{
    Type, Universe, product::ProductType, coproduct::CoproductType, function::FunctionType
};

/// A raw isotope type, without associated metadata
#[derive(Debug, Clone, PartialEq)]
#[derive(From)]
pub enum ConstantType<'tcx> {
    /// A given level of typing universe
    Universe(Universe),
    /// A product type
    Product(ProductType<ConstantType<'tcx>, Bore<'tcx, [ConstantType<'tcx>]>>),
    /// A coproduct type
    Coproduct(CoproductType<ConstantType<'tcx>, Bore<'tcx, [ConstantType<'tcx>]>>),
    /// A function type
    Function(FunctionType<
        ConstantType<'tcx>, Bore<'tcx, [ConstantType<'tcx>]>, Bore<'tcx, ConstantType<'tcx>>
        >),
    /// The type of bytes
    Byte,
    /// The type of sequences of bytes
    Bytes,
    /// The type of strings
    String
}

impl<'tcx> Type for ConstantType<'tcx> {
    type TypeContext = ();

    #[inline]
    fn level(&self, n : &Self::TypeContext) -> Universe {
        match self {
            ConstantType::Universe(u) => u.level(n),
            ConstantType::Product(p) => p.level(n),
            ConstantType::Coproduct(s) => s.level(n),
            ConstantType::Function(f) => f.level(n),
            ConstantType::Byte => Universe::set(),
            ConstantType::Bytes => Universe::set(),
            ConstantType::String => Universe::set()
        }
    }
}
