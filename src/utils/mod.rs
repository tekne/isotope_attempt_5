use std::sync::Arc;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum EitherRef<T, S> {
    Left(T),
    Right(S)
}

impl<T, S, R: ?Sized> AsRef<R> for EitherRef<T, S> where T: AsRef<R>, S: AsRef<R> {
    fn as_ref(&self) -> &R {
        match self {
            EitherRef::Left(l) => l.as_ref(),
            EitherRef::Right(r) => r.as_ref()
        }
    }
}

#[derive(Debug, Eq, PartialEq)]
pub struct WrappedRef<'ctx, T: ?Sized>(pub &'ctx T);

impl<'ctx, T: ?Sized> Clone for WrappedRef<'ctx, T> {
    fn clone(&self) -> Self { WrappedRef(self.0) }
}

impl<'ctx, T: ?Sized> Copy for WrappedRef<'ctx, T> {}

impl<'ctx, T: ?Sized> AsRef<T> for WrappedRef<'ctx, T> {
    fn as_ref(&self) -> &T { self.0 }
}

impl<'ctx, T: ?Sized, S> From<&'ctx T> for EitherRef<WrappedRef<'ctx, T>, S> {
    fn from(r : &'ctx T) -> Self { EitherRef::Left(WrappedRef(r)) }
}

pub type Bore<'ctx, T> = EitherRef<WrappedRef<'ctx, T>, Arc<T>>;

impl<'ctx, T: ?Sized> From<Arc<T>> for Bore<'ctx, T> {
    fn from(r : Arc<T>) -> Self { EitherRef::Right(r) }
}
