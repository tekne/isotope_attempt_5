use std::fmt::Debug;

use super::{Type, Universe};

/// A product type
#[derive(Debug)]
pub struct ProductType<T, S> where T: Type, S: AsRef<[T]> {
    val : S,
    marker : std::marker::PhantomData<fn(T) -> T>
}

impl<T: Type, S: AsRef<[T]>> ProductType<T, S> {
    /// Create a new Product type from the given data
    pub fn new<D: Into<S>>(ty : D) -> ProductType<T, S> {
        ProductType { val : ty.into(), marker : std::marker::PhantomData }
    }
    /// Get an iterator over the types in this product
    pub fn types(&self) -> impl Iterator<Item=&T> { self.val.as_ref().iter() }
}

impl<T: 'static, S> ProductType<T, S> where T: Type, S: AsRef<[T]> + From<&'static [T]> {
    pub fn unit() -> ProductType<T, S> {
        ProductType { val : (&[][..]).into(), marker : std::marker::PhantomData }
    }
}

impl<T, S> PartialEq for ProductType<T, S> where T: Type + PartialEq, S: AsRef<[T]> {
    fn eq(&self, other : &Self) -> bool { self.val.as_ref() == other.val.as_ref() }
}

impl<T, S> Clone for ProductType<T, S> where T: Type, S: AsRef<[T]> + Clone {
    fn clone(&self) -> Self {
        ProductType { val : self.val.clone(), marker : std::marker::PhantomData }
    }
}

impl<T, S> Type for ProductType<T, S> where T: Type + PartialEq, S: AsRef<[T]> + Debug {
    type TypeContext = T::TypeContext;

    fn level(&self, ctx : &Self::TypeContext) -> Universe {
        self.types().map(|t| t.level(ctx)).max().unwrap_or(Universe::set())
    }
}
