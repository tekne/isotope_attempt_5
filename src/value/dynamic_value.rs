/*!
An interface for dynamic pointers to a particular class of values
*/

use crate::typing::{Type, Universe};
use super::Value;

/// An interface for dynamic pointers to a particular class of values
pub trait DynamicValue<TypeKind, TypingContext> {
    /// Whether this value is a type, and if so, what level it is
    fn dyn_type_level(&self, ctx : &TypingContext) -> Option<Universe>;
    /// Get the type of this value
    fn dyn_get_type(&self, ctx : &TypingContext) -> TypeKind;
    /// Whether this value is a type
    fn dyn_is_type(&self, ctx : &TypingContext) -> bool;
}

impl<T, K, C> DynamicValue<K, C> for T where T: Value<TypeKind=K, TypingContext=C>, K: Type {
    #[inline] fn dyn_type_level(&self, ctx : &C) -> Option<Universe> { self.type_level(ctx) }
    #[inline] fn dyn_get_type(&self, ctx : &C) -> K { self.get_type(ctx) }
    #[inline] fn dyn_is_type(&self, ctx : &C) -> bool { self.is_type(ctx) }
}
