/*!
A scope in which values can be defined, imported into and imported from.
Used in defining modules, blocks, etc.
*/
