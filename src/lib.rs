/*!
The isotope intermediate representation.
A prototype for RAIN (RAIN is an Abstract Intermediate Notation)
*/

pub mod typing;
pub mod value;
pub mod block;
pub mod utils;
