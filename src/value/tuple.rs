/*!
A value consisting of a tuple of values, which can be shared or referenced
*/

use std::sync::Arc;
use std::iter::FromIterator;
use std::cmp::Ordering;
use std::fmt::Debug;
use lazycell::AtomicLazyCell;

use crate::typing::{product::ProductType, Universe, Type};
use crate::utils::Bore;
use super::{Value};

/// A tuple of values
#[derive(Debug)]
pub struct Tuple<V, S, ST, T = <V as Value>::TypeKind> where
    V: Value,
    S: AsRef<[V]>,
    ST: AsRef<[T]>,
    T: Type + From<V::TypeKind> {
    val : S,
    cached_type : AtomicLazyCell<ProductType<T, ST>>,
    marker : std::marker::PhantomData<fn() -> V>
}

impl<V, S, ST, T> Tuple<V, S, ST, T> where
    V: Value,
    S: AsRef<[V]>,
    ST: AsRef<[T]>,
    T: Type + From<V::TypeKind> {
    /// Create a new tuple from the given data
    #[inline]
    pub fn new<D>(tup : D) -> Self where S : From<D> {
        Tuple {
            val : S::from(tup) ,
            cached_type : AtomicLazyCell::new(),
            marker : std::marker::PhantomData
        }
    }
}

impl<V, S, ST, T> Clone for Tuple<V, S, ST, T> where
    V: Value,
    S: AsRef<[V]> + Clone,
    ST: AsRef<[T]> + Clone,
    T: Type + From<V::TypeKind> {
    #[inline]
    fn clone(&self) -> Self {
        Tuple {
            val : self.val.clone(),
            cached_type : self.cached_type.clone(),
            marker : std::marker::PhantomData
        }
    }
}

impl<V, S, ST, T> PartialEq for Tuple<V, S, ST, T> where
    V: Value + PartialEq,
    S: AsRef<[V]>,
    ST: AsRef<[T]>,
    T: Type + From<V::TypeKind> {
    #[inline]
    fn eq(&self, other : &Self) -> bool {
        self.val.as_ref().eq(other.val.as_ref())
    }
    #[inline]
    fn ne(&self, other : &Self) -> bool {
        self.val.as_ref().ne(other.val.as_ref())
    }
}

impl<V, S, ST, T> Tuple<V, S, ST, T> where
    V: 'static + Value,
    S: AsRef<[V]> + From<&'static [V]>,
    ST: AsRef<[T]>,
    T: Type + From<V::TypeKind> {
    /// Create a new tuple from the given data
    pub fn null() -> Self {
        Tuple {
            val : S::from(&[][..]),
            cached_type : AtomicLazyCell::new(),
            marker : std::marker::PhantomData
        }
    }
}


impl<V, S, ST, T> Value for Tuple<V, S, ST, T> where
    V: Value,
    S: AsRef<[V]> + Debug,
    ST: AsRef<[T]> + From<Arc<[T]>> + Debug, //TODO: clean
    T: Type + From<V::TypeKind>,
    ProductType<T, ST>: Clone {
    type TypeKind = ProductType<T, ST>;
    type TypingContext = V::TypingContext;
    #[inline(always)]
    fn type_level(&self, ctx : &Self::TypingContext) -> Option<Universe> {
        self.val.as_ref().iter().map(|t| t.type_level(ctx)).max_by(|l, r| {
            match l {
                None => Ordering::Less,
                Some(l) => match r {
                    None => Ordering::Greater,
                    Some(r) => l.cmp(r)
                }
            }
        }).unwrap_or(None)
    }
    #[inline]
    fn get_type(&self, ctx : &Self::TypingContext) -> Self::TypeKind {
        match self.cached_type.borrow() {
            Some(r) => r.clone(),
            None => {
                let res_vec = Vec::from_iter(
                    self.val.as_ref().iter().map( |val| T::from(val.get_type(ctx)) )
                );
                let res = ProductType::new(Arc::from(res_vec));
                self.cached_type.fill(res).map_err(|_| ()).expect("Cell should be empty");
                self.cached_type.borrow().unwrap().clone()
            }
        }
    }
}

pub type BoreTuple<'vcx, V> = Tuple<V, Bore<'vcx, [V]>, Bore<'vcx, [<V as Value>::TypeKind]>>;

#[cfg(test)]
mod tests {
    use crate::value::{
        static_value::StaticValue, constant::SimpleConstant, tuple::BoreTuple as Tuple
    };
    use crate::typing::{constant::ConstantType, product::ProductType, Universe};

    #[test]
    fn simple_tuples_have_correct_type() {
        let values : [SimpleConstant; 3] = [
            SimpleConstant::Byte(12),
            (&b"Hello!"[..]).into(),
            "Hello world!".into()
        ];
        let types : [ConstantType; 3] = [
            ConstantType::Byte.into(), ConstantType::Bytes.into(), ConstantType::String.into()
        ];
        let tuple = Tuple::new(&values[..]);
        let tuple_type = ProductType::new(&types[..]);
        let sub_tuple = Tuple::new(&values[1..]);
        let sub_tuple_type = ProductType::new(&types[1..]);
        assert_eq!(tuple.get_type(), tuple_type);
        assert_eq!(sub_tuple.get_type(), sub_tuple_type);
    }
    #[test]
    fn complex_tuples_have_correct_type() {
        let values : [SimpleConstant; 2] = [
            ConstantType::Universe(Universe::set()).into(),
            SimpleConstant::Byte(54)
        ];
        let types : [ConstantType; 2] = [
            Universe::set().next().into(),
            ConstantType::Byte
        ];
        let tuple = Tuple::new(&values[..]);
        let tuple_type = ProductType::new(&types[..]);
        assert_eq!(tuple.get_type(), tuple_type);
    }
}
