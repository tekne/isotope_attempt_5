/*!
Constant isotope values
*/

use std::borrow::Cow;
use derive_more::From;

use crate::typing::{constant::ConstantType, Universe};
use super::Value;

/// An isotope constant value
#[derive(Debug, Clone,  PartialEq)]
#[derive(From)]
pub enum SimpleConstant<'vcx> {
    /// A single byte
    Byte(u8),
    /// A fixed sequence of bytes
    Bytes(Cow<'vcx, [u8]>),
    /// A UTF-8 string value
    String(Cow<'vcx, str>),
    /// A constant type
    Type(ConstantType<'vcx>)
}

impl<'vcx> From<&'vcx [u8]> for SimpleConstant<'vcx> {
    fn from(s : &'vcx [u8]) -> SimpleConstant<'vcx> {
        SimpleConstant::Bytes(s.into())
    }
}

impl<'vcx> From<&'vcx str> for SimpleConstant<'vcx> {
    fn from(s : &'vcx str) -> SimpleConstant<'vcx> {
        SimpleConstant::String(s.into())
    }
}

impl<'vcx> From<String> for SimpleConstant<'vcx> {
    fn from(s: String) -> SimpleConstant<'vcx> {
        SimpleConstant::String(s.into())
    }
}

impl<'vcx> Value for SimpleConstant<'vcx> {
    type TypeKind = ConstantType<'vcx>;
    type TypingContext = ();
    #[inline(always)]
    fn type_level(&self, n : &()) -> Option<Universe> {
        match self {
            SimpleConstant::Type(t) => t.type_level(n),
            _ => None
        }
    }
    fn get_type(&self, n : &()) -> ConstantType<'vcx> {
        match self {
            SimpleConstant::Byte(_) => ConstantType::Byte.into(),
            SimpleConstant::Bytes(_) => ConstantType::Bytes.into(),
            SimpleConstant::String(_) => ConstantType::String.into(),
            SimpleConstant::Type(t) => t.get_type(n).into()
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{SimpleConstant, super::static_value::StaticValue};
    use crate::typing::{constant::ConstantType};
    #[test]
    fn simple_constants_have_correct_type() {
        assert_eq!(SimpleConstant::Byte(21).get_type(), ConstantType::Byte);
        assert_eq!(
            SimpleConstant::from(&b"Hello world!"[..]).get_type(),
            ConstantType::Bytes
        );
        assert_eq!(
            SimpleConstant::from("Hello world!").get_type(),
            ConstantType::String
        );
    }
}
