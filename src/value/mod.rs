/*!
Isotope values
*/

use std::fmt::Debug;

use super::typing::{Type, Universe};

pub mod constant;
pub mod tuple;
pub mod static_value;
pub mod dynamic_value;
pub mod scope;
pub mod expr;
pub mod call;

/// The trait implemented by all isotope values
pub trait Value: Debug {
    /// The kind of type this value has
    type TypeKind : Type;
    /// The typing context this value requires
    type TypingContext;
    /// Whether this value is a type, and if so, what level it is
    fn type_level(&self, ctx : &Self::TypingContext) -> Option<Universe>;
    /// Get the type of this value
    fn get_type(&self, ctx : &Self::TypingContext) -> Self::TypeKind;
    /// Whether this value is a type
    fn is_type(&self, _ctx : &Self::TypingContext) -> bool { false }
}
