/*!
A simplified interface for values which do not require a typing context
(i.e. have `TypingContext=()`)
*/

use crate::typing::{Type, Universe};
use super::Value;

/// An isotope value whose type does not depend on a dynamic typing context
pub trait StaticValue {
    type TypeKind : Type;
    /// Whether this value is a type, and if so, what level it is
    fn type_level(&self) -> Option<Universe>;
    /// Get the type of this value
    fn get_type(&self) -> Self::TypeKind;
    /// Whether this value is a type
    fn is_type(&self) -> bool;
}

impl<T: Value<TypingContext = ()>> StaticValue for T {
    type TypeKind = <T as Value>::TypeKind;
    fn type_level(&self) -> Option<Universe> {
        <Self as Value>::type_level(self, &())
    }
    fn get_type(&self) -> Self::TypeKind {
        <Self as Value>::get_type(self, &())
    }
    fn is_type(&self) -> bool {
        <Self as Value>::is_type(self, &())
    }
}
