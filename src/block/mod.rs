/*!
Basic blocks, which form the fundamental component of isotope CFGs,
which are used to define lambda functions, computations and formal
proofs.
*/

use typed_generational_arena::{SmallArena as Arena, SmallIndex as ArenaIndex};
use derive_more::From;

use crate::utils::Bore;
use crate::value::{Value, constant::SimpleConstant};
use crate::typing::{constant::ConstantType, product::ProductType, Universe};

/// A value in an isotope basic block
#[derive(Debug, Clone)]
#[derive(From)]
pub enum BlockValue<'vcx> {
    /// A constant value, inherited from the value context
    Constant(SimpleConstant<'vcx>),
    /// A parameter to the function
    Parameter(usize),
    /// A value ID
    ID(ValueIdx<'vcx>)
}

/// The type kind for block values
pub type BlockType<'vcx> = ConstantType<'vcx>; //TODO: this

impl<'vcx> Value for BlockValue<'vcx> {
    //TODO: this
    type TypeKind = BlockType<'vcx>;
    type TypingContext = Block<'vcx>;
    fn type_level(&self, ctx : &Block<'vcx>) -> Option<Universe> {
        use BlockValue::*;
        match self {
            Constant(c) => c.type_level(&()),
            Parameter(_p) => unimplemented!(),
            ID(i) => ctx.values[*i].type_level(ctx)
        }
    }
    fn get_type(&self, ctx : &Block<'vcx>) -> Self::TypeKind {
        use BlockValue::*;
        match self {
            Constant(c) => c.get_type(&()),
            Parameter(_p) => unimplemented!(),
            ID(i) => ctx.values[*i].get_type(ctx)
        }
    }
    fn is_type(&self, ctx : &Block<'vcx>) -> bool {
        use BlockValue::*;
        match self {
            Constant(c) => c.is_type(&()),
            Parameter(_p) => unimplemented!(),
            ID(i) => ctx.values[*i].is_type(ctx)
        }
    }
}

/// The index of a value in an isotope basic block
pub type ValueIdx<'vcx> = ArenaIndex<BlockValue<'vcx>>;

/// An incomplete isotope basic block
#[derive(Debug, Clone)]
pub struct BlockBuilder<'vcx> {
    values : Arena<BlockValue<'vcx>>,
    parameters : ProductType<BlockType<'vcx>, Bore<'vcx, [BlockType<'vcx>]>>,
    terminator : Option<ValueIdx<'vcx>>
}

impl<'vcx> BlockBuilder<'vcx> {
    /// Create a new, empty block builder given parameters
    pub fn new(parameters : ProductType<BlockType<'vcx>, Bore<'vcx, [BlockType<'vcx>]>>)
    -> BlockBuilder<'vcx> {
        BlockBuilder { values : Arena::new(), terminator : None, parameters }
    }
    /// Attempt to build a basic block. Returns self on failure.
    pub fn build(self) -> Result<Block<'vcx>, BlockBuilder<'vcx>> {
        match self.terminator {
            Some(term) => Ok(Block {
                values : self.values, parameters : self.parameters, terminator : term
            }),
            None => Err(self)
        }
    }
}


/// An isotope basic block
#[derive(Debug, Clone)]
pub struct Block<'vcx> {
    values : Arena<BlockValue<'vcx>>,
    parameters : ProductType<BlockType<'vcx>, Bore<'vcx, [BlockType<'vcx>]>>,
    terminator : ValueIdx<'vcx>
}
