/*!
Function call expressions
*/
use super::Value;

pub struct Call<V, SF, SA> where V: Value, SF: AsRef<V>, SA: AsRef<[V]> {
    called : SF,
    callee : SA,
    marker : std::marker::PhantomData<fn() -> V>
}

impl<V, SF, SA, F, A> From<(F, A)> for Call<V, SF, SA> where
    V: Value,
    SF: AsRef<V> + From<F>,
    SA: AsRef<[V]> + From<A> {
    fn from(tup : (F, A)) -> Self {
        Call { called : tup.0.into(), callee : tup.1.into(), marker : std::marker::PhantomData }
    }
}
