use std::fmt::Debug;

use super::{Type, Universe};

/// A coproduct type
#[derive(Debug)]
pub struct CoproductType<T, S> where T: Type, S: AsRef<[T]> {
    val : S,
    marker : std::marker::PhantomData<fn(T) -> T>
}

impl<T: Type, S: AsRef<[T]>> CoproductType<T, S> {
    /// Create a new coproduct type from the given data
    pub fn new<D: Into<S>>(ty : D) -> CoproductType<T, S> {
        CoproductType { val : ty.into(), marker : std::marker::PhantomData }
    }
    /// Get an iterator over the types in this product
    pub fn types(&self) -> impl Iterator<Item=&T> { self.val.as_ref().iter() }
}

impl<T, S> PartialEq for CoproductType<T, S> where T: Type + PartialEq, S: AsRef<[T]> {
    fn eq(&self, other : &Self) -> bool { self.val.as_ref() == other.val.as_ref() }
}

impl<T, S> Clone for CoproductType<T, S> where T: Type, S: AsRef<[T]> + Clone {
    fn clone(&self) -> Self {
        CoproductType { val : self.val.clone(), marker : std::marker::PhantomData }
    }
}

impl<T, S> Type for CoproductType<T, S> where T: Type + PartialEq, S: AsRef<[T]> + Debug {
    type TypeContext = T::TypeContext;

    fn level(&self, ctx : &Self::TypeContext) -> Universe {
        self.types().map(|t| t.level(ctx)).max().unwrap_or(Universe::set())
    }
}
