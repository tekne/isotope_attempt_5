/*!
The basics of the isotope type system
*/

use std::num::NonZeroUsize;
use std::fmt::Debug;
use derive_more::{From, Into};

use super::value::Value;

pub mod product;
pub mod coproduct;
pub mod function;
pub mod constant;

/// The trait implemented by all isotope types
pub trait Type: Debug + PartialEq {
    /// The context this type requires
    type TypeContext;
    /// The level of this type in the type hierarchy
    fn level(&self, ctx : &Self::TypeContext) -> Universe;
}

impl<T: Type> Value for T {
    type TypeKind = Universe;
    type TypingContext = <Self as Type>::TypeContext;
    #[inline(always)]
    fn type_level(&self, ctx : &Self::TypingContext) -> Option<Universe> {
        Some(self.level(ctx))
    }
    #[inline(always)]
    fn get_type(&self, ctx : &Self::TypingContext) -> Universe { self.level(ctx) }
}

/// A typing universe
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
#[derive(From, Into)]
pub struct Universe {
    univ : NonZeroUsize
}

impl Universe {
    /// The universe of simple values
    #[inline(always)]
    pub fn set() -> Universe {
        NonZeroUsize::new(1).unwrap().into()
    }
    /// The universe containing this universe
    #[inline(always)]
    pub fn next(self) -> Universe {
        NonZeroUsize::new(self.univ.get() + 1).unwrap().into()
    }
}

impl Type for Universe {
    type TypeContext = ();
    #[inline(always)]
    fn level(&self, _ : &()) -> Universe { self.next() }
}
