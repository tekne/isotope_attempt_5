use std::fmt::Debug;
use super::{Type, Universe, product::ProductType};

/// A function type
#[derive(Debug, Clone)]
pub struct FunctionType<T, SA, SR> where
    T: Type,
    SA: AsRef<[T]>,
    SR: AsRef<T> {
    arguments : ProductType<T, SA>,
    result : SR
}

impl<T, SA, SR> PartialEq for FunctionType<T, SA, SR> where
    T: Type + PartialEq,
    SA: AsRef<[T]>, SR: AsRef<T> {
    fn eq(&self, other : &Self) -> bool {
        self.result.as_ref().eq(other.result.as_ref())
        && self.arguments == other.arguments
    }
}

impl<T, SA, SR> Type for FunctionType<T, SA, SR> where
    T: Type + PartialEq,
    SA: AsRef<[T]> + Debug,
    SR: AsRef<T> + Debug,
    ProductType<T, SA> : Type<TypeContext=T::TypeContext> {
    type TypeContext = T::TypeContext;

    fn level(&self, ctx : &Self::TypeContext) -> Universe {
        std::cmp::max(self.result.as_ref().level(ctx), self.arguments.level(ctx))
    }
}
